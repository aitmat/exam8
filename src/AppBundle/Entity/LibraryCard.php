<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * LibraryCard
 *
 * @ORM\Table(name="library_card")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LibraryCardRepository")
 */
class LibraryCard
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="fullName", type="string")
     */
    private $fullName;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string")
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="password_id", type="string")
     */
    private $passwordId;

    /**
     * @var string
     *
     * @ORM\Column(name="ticket_id", type="string")
     */
    private $ticketId;

    /**
     * @ORM\ManyToMany(targetEntity="Book", mappedBy="readers")
     */
    private $books;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fullName
     *
     * @param string $fullName
     *
     * @return LibraryCard
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;

        return $this;
    }

    /**
     * Get fullName
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return LibraryCard
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set passwordId
     *
     * @param string $passwordId
     *
     * @return LibraryCard
     */
    public function setPasswordId($passwordId)
    {
        $this->passwordId = $passwordId;

        return $this;
    }

    /**
     * Get passwordId
     *
     * @return string
     */
    public function getPasswordId()
    {
        return $this->passwordId;
    }

    /**
     * Set ticketId
     *
     * @param string $ticketId
     *
     * @return LibraryCard
     */
    public function setTicketId($ticketId)
    {
        $this->ticketId = $ticketId;

        return $this;
    }

    /**
     * Get ticketId
     *
     * @return string
     */
    public function getTicketId()
    {
        return $this->ticketId;
    }

    /**
     * @param mixed $books
     * @return LibraryCard
     */
    public function setBooks($books)
    {
        $this->books = $books;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBooks()
    {
        return $this->books;
    }
}

