<?php

namespace AppBundle\Controller;

use AppBundle\Entity\LibraryCard;
use AppBundle\Entity\Proposal;
use AppBundle\Form\ProposalType;
use AppBundle\Form\TicketType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class BasicController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $genres = $this->getDoctrine()
            ->getRepository('AppBundle:Category')
            ->findAll();

        $books = $this->getDoctrine()
            ->getRepository('AppBundle:Book')
            ->findAll();

        return $this->render('@App/Basic/index.html.twig', [
            'genres' => $genres,
            'books' => $books,
        ]);
    }

    /**
     * @Route("/login_admin")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loginAdminAction(Request $request)
    {
        $genres = $this->getDoctrine()
            ->getRepository('AppBundle:Category')
            ->findAll();

        $books = $this->getDoctrine()
            ->getRepository('AppBundle:Book')
            ->findAll();

        return $this->render('@App/Basic/index.html.twig', [
            'genres' => $genres,
            'books' => $books,
        ]);
    }

    /**
     * @Route("/category/{id}", name="category", requirements={"id": "\d+"})
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function categoryAction(int $id)
    {
        $genres = $this->getDoctrine()
            ->getRepository('AppBundle:Category')
            ->findAll();

        $books = $this->getDoctrine()
            ->getRepository('AppBundle:Book')
            ->findeByJanre($id);

        return $this->render('@App/Basic/index.html.twig', [
            'genres' => $genres,
            'books' => $books,
        ]);
    }

    /**
     * @Route("/new_ticket", name="ticket")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createTicketAction(Request $request, EntityManagerInterface $em)
    {
        $ticket = new LibraryCard();
        $form = $this->createForm(TicketType::class, $ticket);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $idPass = $ticket->getPasswordId();

            $isset = $this->getDoctrine()
                ->getRepository('AppBundle:LibraryCard')
                ->checkPassword($idPass);

            if ($isset){
                $this->addFlash('error', 'error_about_pass');
                return $this->redirectToRoute('ticket');
            }

            $ticket->setTicketId(uniqid());
            $em->persist($ticket);
            $em->flush();
            return $this->render('@App/Basic/successRegistr.html.twig', [
                'ticket' => $ticket,
            ]);
        }

        return $this->render('@App/Basic/newticket.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{_locale}", name="locale", requirements = {"_locale" : "en|ru"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function changeLocaleAction(Request $request)
    {
        return $this->redirect($request->server->get('HTTP_REFERER'));
    }

    /**
     * @Route("/some", name="some")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function proposalAction(Request $request, EntityManagerInterface $em)
    {
        $proposal = new Proposal();
        $form = $this->createForm(ProposalType::class, $proposal);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $id_ticket = $proposal->getTicketId();

            $reader = $this->getDoctrine()
                    ->getRepository('AppBundle:LibraryCard')
                    ->checkTicketId($id_ticket);

            if (!$reader){
                $this->addFlash('error', 'error_about_pass');
                return $this->redirect($request->server->get('HTTP_REFERER'));
            }
            $em->persist($proposal);
            $em->flush();
        }
            return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/proposal/{id}", name="proposal", requirements={"id": "\d+"})
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function formProposalAction(int $id)
    {
        $book = $this->getDoctrine()
            ->getRepository('AppBundle:Book')
            ->find($id);

        $form = $this->createForm(ProposalType::class, new Proposal());

        return $this->render('@App/Basic/doProposal.html.twig', [
            'book' => $book,
            'form' => $form->createView(),
        ]);
    }
}
